Rails.application.routes.draw do
  
  #Redirections
  get '/admin', to: redirect('/admins/sign_in')
  
  # Devise routes
  devise_for :admins, controllers: { sessions: "sessions/admin" }
  devise_for :users,  controllers: { sessions: "sessions/user", registrations: "registrations/user" }
  
  # Admin routes
  namespace :admin, shallow: true do
    resources :categories do
      get 'status', on: :member
      resources :groups do
        resources :enrollments do
          resources :payments
        end
      end
    end
    resources :all_payments do
      get 'status', on: :member
    end
    resources :users
  end
  
  # Public routes
  resources :organizations, only: [:index], shallow: true do
    resources :categories do
      resources :groups do
        resources :enrollments, only: [:new, :create]
      end
    end
  end
  
  # Private routes
  resources :enrollments, except: [:new, :create], shallow: true do
    resources :payments
  end
  
  root "organizations#index"
end
