require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  setup do
    @organization = organizations(:one)
    @category = categories(:one)
    @params = { 
      name: 'Categoría Foo',
      organization: @organization,
      position: 2,
      status: false
    }
  end

  test "should get index" do
    get :index, organization_id: @organization
    assert_response :success
    assert_not_nil assigns(:categories)
  end

  test "should get new" do
    get :new, organization_id: @organization
    assert_response :success
  end

  test "should create category" do
    assert_difference('Category.count') do
      post :create, organization_id: @organization, category: @params
    end

    assert_redirected_to category_path(assigns(:category))
  end

  test "should get edit" do
    get :edit, id: @category
    assert_response :success
  end

  test "should update category" do
    patch :update, id: @category, category: @params
    assert_redirected_to category_path(assigns(:category))
  end

  test "should destroy category" do
    assert_difference('Category.count', -1) do
      delete :destroy, id: @category
    end

    assert_redirected_to organization_categories_path(@organization)
  end
end
