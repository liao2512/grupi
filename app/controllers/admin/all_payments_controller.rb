class Admin::AllPaymentsController < Admin::AdminController
  before_action :set_payment, only: [:edit, :update, :toggle_status, :destroy]

  def index
    @payments = @org.payments
  end

  def edit
  end

  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to admin_all_payments_path, notice: 'Pago actualizado' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to admin_all_payments_url, notice: 'Pago eliminado' }
      format.json { head :no_content }
    end
  end

  private
    def set_payment
      @payment = Payment.find(params[:id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @payment.enrollment.group.category.organization != @org
    end

    def payment_params
      params.require(:payment).permit(:amount, :status, :date, :bank, :reference, :owner, :concept, :comments, :enrollment_id)
    end
end
