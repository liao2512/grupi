class Admin::CategoriesController < Admin::AdminController
  before_action :set_categories, only: [:index, :new, :edit]
  before_action :set_category,   only: [:show, :edit, :update, :destroy, :status]

  def index
  end

  def show
  end
  
  def status
    @category.toggle :status
    @category.save
    @categories = @org.categories
  end

  def new
    @category = Category.new
  end

  def edit
  end

  def create
    @categories = @org.categories.where("position >= ?", params[:category][:position]).to_a
    @category = Category.new(category_params)
    @category.organization = @org

    respond_to do |format|
      if @category.save
        @categories.each do |category|
          category.increment!(:position)
        end
        format.html { redirect_to admin_categories_path, notice: "Categoría #{@category.name.titleize} creada" }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    new_position = params[:category][:position].to_i
    old_position = @category.position.to_i
    if new_position < old_position
      @categories = @org.categories.where(position: new_position..old_position-1).to_a
    elsif new_position > old_position
      @categories = @org.categories.where(position: old_position+1..new_position).to_a
    end
    if @category.update(category_params)
      if new_position < old_position
        @categories.each do |category|
          category.increment!(:position)
        end
      elsif new_position > old_position
        @categories.each do |category|
          category.decrement!(:position)
        end
      end
      redirect_to admin_categories_path, notice: "Categoría #{@category.name.titleize} actualizada"
    else
      render :edit
    end
  end

  def destroy
    @category.destroy
    @categories = @org.categories.where("position >= ?", @category.position)
    @categories.each do |category|
      category.decrement!(:position)
    end
    redirect_to admin_categories_url, notice: "Categoría #{@category.name.titleize} eliminada"
  end

  private
    def set_categories
      @categories = @org.categories
    end
    
    def set_category
      @category = Category.find(params[:id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @category.organization != @org
    end

    def category_params
      params.require(:category).permit(:position, :name, :status, :organization_id)
    end
end
