class Admin::GroupsController < Admin::AdminController
  before_action :set_category,      only: [:index, :new, :create]
  before_action :set_group,         only: [:show, :edit, :update, :destroy]
  before_action :set_groups_count,  only: [:new, :edit]

  def index
    @groups = @category.groups
  end

  def show
  end

  def new
    @group = Group.new
  end

  def edit
  end

  def create
    @groups = @category.groups.where("position >= ?", params[:group][:position]).to_a
    @group = Group.new(group_params)
    @group.category = @category

    respond_to do |format|
      if @group.save
        @groups.each do |group|
          group.increment!(:position)
        end
        format.html { redirect_to admin_category_groups_path(@category), notice: "#{@group.name.titleize} creado" }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    new_position = params[:group][:position].to_i
    old_position = @group.position.to_i
    if new_position < old_position
      @groups = @group.category.groups.where(position: new_position..old_position-1).to_a
    elsif new_position > old_position
      @groups = @group.category.groups.where(position: old_position+1..new_position).to_a
    end
    if @group.update(group_params)
      if new_position < old_position
        @groups.each do |group|
          group.increment!(:position)
        end
      elsif new_position > old_position
        @groups.each do |group|
          group.decrement!(:position)
        end
      end
      redirect_to admin_category_groups_path(@group.category), notice: "#{@group.name.titleize} actualizado"
    else
      render :edit
    end
  end

  def destroy
    @group.destroy
    @groups = @group.category.groups.where("position >= ?", @group.position)
    @groups.each do |group|
      group.decrement!(:position)
    end
    redirect_to admin_category_groups_url(@group.category), notice: "#{@group.name.titleize} eliminado"
  end

  private
    def set_category
      @category = Category.find(params[:category_id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @category.organization != @org
    end
    
    def set_groups_count
      if @category
        @groups_count = @category.groups.count
      else
        @groups_count = @group.category.groups.count
      end
    end
    
    def set_group
      @group = Group.find(params[:id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @group.category.organization != @org
    end

    def group_params
      params.require(:group).permit(:position, :name, :description, :places, :status, :category_id)
    end
end
