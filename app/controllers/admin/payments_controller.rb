class Admin::PaymentsController < Admin::AdminController
  before_action :set_enrollment, only: [:index, :new, :create]
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  def index
    @payments = @enrollment.payments.all
  end

  def show
  end

  def new
    @payment = Payment.new
  end

  def edit
  end

  def create
    @payment = Payment.new(payment_params)
    @payment.enrollment = @enrollment

    respond_to do |format|
      if @payment.save
        format.html { redirect_to enrollment_payments_path(@enrollment), notice: 'Pago reportado' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to admin_enrollment_payments_path(@payment.enrollment), notice: 'Pago actualizado' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to admin_enrollment_payments_url(@payment.enrollment), notice: 'Pago eliminado' }
      format.json { head :no_content }
    end
  end

  private
    def set_enrollment
      @enrollment = Enrollment.find(params[:enrollment_id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @enrollment.group.category.organization != @org
    end
    
    def set_payment
      @payment = Payment.find(params[:id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @payment.enrollment.group.category.organization != @org
    end
    
    def payment_params
      params.require(:payment).permit(:amount, :status, :date, :bank, :reference, :owner, :concept, :comments, :enrollment_id)
    end
end
