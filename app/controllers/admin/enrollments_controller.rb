class Admin::EnrollmentsController < Admin::AdminController
  before_action :set_group,      only: [:index, :new, :create]
  before_action :set_enrollment, only: [:show, :edit, :update, :destroy]

  def index
    @enrollments = @group.enrollments
  end

  def update
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to admin_group_enrollments_path(@enrollment.group), notice: 'Inscripción actualizada' }
        format.json { render :show, status: :ok, location: @enrollment }
      else
        format.html { render :edit }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @enrollment.destroy
    respond_to do |format|
      format.html { redirect_to enrollments_url, notice: 'Inscripción eliminada' }
      format.json { head :no_content }
    end
  end

  private
    def set_group
      @group = Group.find(params[:group_id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @group.category.organization != @org
    end
    
    def set_enrollment
      @enrollment = Enrollment.find(params[:id])
      redirect_to admin_categories_url, notice: 'Acceso no permitido' if @enrollment.group.category.organization != @org
    end

    def enrollment_params
      params.require(:enrollment).permit(
        :first_name, :last_name, :birthdate, :status, :id_number, :email, :phone, :mobile, :address,
        :occupation, :refered_by, :comments, payments_attributes: 
        [ :id, :amount, :status, :date, :bank, :reference, :owner, :concept, :comments ])
    end
end
