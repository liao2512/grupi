class Admin::AdminController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_organization
  
  protected
    def set_organization
      @org = current_admin.organization
    end
end
