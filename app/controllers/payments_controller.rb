class PaymentsController < ApplicationController
  before_action :set_enrollment, only: [:index, :new, :create]
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  def index
    @payments = @enrollment.payments.all
  end

  def show
  end

  def new
    @payment = Payment.new
  end

  def edit
  end

  def create
    @payment = Payment.new(payment_params)
    @payment.enrollment = @enrollment
    @payment.status = 0

    if @payment.save
      redirect_to enrollment_payments_path(@enrollment), notice: 'Pago reportado'
    else
      render :new
    end
  end

  def update
    if @payment.update(payment_params)
      redirect_to enrollment_payments_path(@payment.enrollment), notice: 'Pago actualizado'
    else
      render :edit
    end
  end

  def destroy
    @payment.destroy
    redirect_to enrollment_payments_url(@payment.enrollment), notice: 'Pago eliminado'
  end

  private
    def set_enrollment
      @enrollment = Enrollment.find(params[:enrollment_id])
      redirect_to enrollments_url, notice: 'Acceso no permitido' if @enrollment.user != current_user
    end
    
    def set_payment
      @payment = Payment.find(params[:id])
      redirect_to enrollments_url, notice: 'Acceso no permitido' if @payment.enrollment.user != current_user
    end

    def payment_params
      params.require(:payment).permit(:amount, :status, :date, :bank, :reference, :owner, :concept, :comments, :enrollment_id)
    end
end
