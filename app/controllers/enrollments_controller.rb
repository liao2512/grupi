class EnrollmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_group, only: [:new, :create]
  before_action :set_enrollment, only: [:show, :edit, :update, :destroy]

  def index
    @enrollments = current_user.enrollments
  end

  def show
  end

  def new
    @enrollment = Enrollment.new
    @enrollment.payments.build
  end

  def edit
  end

  def create
    @enrollment = Enrollment.new(enrollment_params)
    @enrollment.user  = current_user
    @enrollment.group = @group
    @enrollment.status = 0

    respond_to do |format|
      if @enrollment.save
        format.html { redirect_to enrollments_path, notice: "Te has inscrito en #{@enrollment.group.name.titleize}" }
        format.json { render :show, status: :created, location: @enrollment }
      else
        format.html { render :new }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to @enrollment, notice: 'Enrollment was successfully updated.' }
        format.json { render :show, status: :ok, location: @enrollment }
      else
        format.html { render :edit }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @enrollment.destroy
    respond_to do |format|
      format.html { redirect_to enrollments_url, notice: 'Enrollment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_group
      @group = Group.find(params[:group_id])
    end
    
    def set_enrollment
      @enrollment = Enrollment.find(params[:id])
    end

    def enrollment_params
      params.require(:enrollment).permit(
        :first_name, :last_name, :birthdate, :status, :id_number, :email, :phone, :mobile, :address,
        :occupation, :refered_by, :comments, payments_attributes: 
        [ :id, :amount, :status, :date, :bank, :reference, :owner, :concept, :comments ])
    end
end
