class Registrations::UserController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)
    enrollments_path
  end
  
  def after_update_path_for(resource)
    enrollments_path
  end
end