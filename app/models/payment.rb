class Payment < ActiveRecord::Base
  belongs_to :enrollment
  
  default_scope { order(date: :desc) }
end
