class Category < ActiveRecord::Base
  belongs_to :organization
  has_many :groups, dependent: :destroy
  
  validates :name, :position, presence: true
  validates :name, uniqueness: { scope: :organization,
    message: "debe ser única por organización" }
  validates :position, numericality: {greater_than_or_equal_to: 1}
  
  default_scope { order(position: :asc) }
  
end
