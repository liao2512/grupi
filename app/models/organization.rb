class Organization < ActiveRecord::Base
  has_many :admins, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :groups, through: :categories
  has_many :enrollments, through: :groups
  has_many :payments, through: :enrollments
end
