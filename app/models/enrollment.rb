class Enrollment < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  has_many :payments, dependent: :destroy
  
  accepts_nested_attributes_for :payments, allow_destroy: true
  
  default_scope { order(first_name: :asc) }
  
  def full_name
    [first_name, last_name].select(&:present?).join(' ').titleize
  end
end
