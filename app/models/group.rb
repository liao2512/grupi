class Group < ActiveRecord::Base
  belongs_to :category
  has_many   :enrollments, dependent: :destroy
  
  default_scope { order(position: :asc) }
end
