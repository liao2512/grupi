class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :position
      t.string :name
      t.boolean :status, null: false, default: false
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
