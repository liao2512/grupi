class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.integer :status, null: false, default: 0
      t.string :id_number
      t.string :email
      t.string :phone
      t.string :mobile
      t.string :address
      t.string :occupation
      t.string :refered_by
      t.text :comments
      t.references :user, index: true, foreign_key: true
      t.references :group, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
