class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.decimal :amount, precision: 12, scale: 2
      t.integer :status, null: false, default: 0
      t.date :date
      t.string :bank
      t.string :reference
      t.string :owner
      t.string :concept
      t.text :comments
      t.references :enrollment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
