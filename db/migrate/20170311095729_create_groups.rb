class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.integer :position
      t.string :name
      t.text :description
      t.integer :places
      t.boolean :status, null: false, default: false
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
